﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Gomoku : ContentPage
    {
        ImageSource logo = ImageSource.FromFile("iicologo");
        ImageSource krizek = ImageSource.FromFile("krizek");
        ImageSource kolecko = ImageSource.FromFile("kolecko");
        List<Image> imgs = new List<Image>();
        bool player = true;
        int[,] plane;
        int reachCount = 4;
        public Gomoku(object sender)
        {
            InitializeComponent();
            int col;
            int row;
            Image imag = sender as Image;
            col =Convert.ToInt32( DeviceDisplay.MainDisplayInfo.Width / 100);
            row = Convert.ToInt32((DeviceDisplay.MainDisplayInfo.Height-50) / 100);
            if (imag.ClassId == "easy")
            {
                reachCount = 2;
                col = 3;
                row = 3;
            }
            if (imag.ClassId == "medium")
            {
                reachCount = 3;
            }
                plane = new int[col, row];
            for (int i = 0; i < col; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }
            for (int i = 0; i < row; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            for (int i = 0; i < col; i++)
            {
                for (int l = 0; l < row; l++)
                {
                    Image img = new Image();
                    imgs.Add(img);
                    img.BackgroundColor = Color.White;
                    TapGestureRecognizer tr = new TapGestureRecognizer();
                    img.GestureRecognizers.Add(tr);
                    tr.Tapped += TapGestureRecognizer_Tapped;
                    grid.Children.Add(img, i, l);
                }
            }
        }
        async private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Image bt = sender as Image;
            int c = (int)bt.GetValue(Grid.ColumnProperty);
            int r = (int)bt.GetValue(Grid.RowProperty);
            if (plane[c,r] == 0)
            {
                if (player)
                {
                    bt.Source = krizek;
                    lbl_x.Text = Convert.ToInt32(lbl_x.Text) + 1 + "";
                    cp_o.BackgroundColor = Color.Green;
                    cp_x.BackgroundColor = Color.White;
                    plane[c, r] = 1;
                }
                else
                {
                    bt.Source = kolecko;
                    lbl_o.Text = Convert.ToInt32(lbl_o.Text) + 1 + "";
                    cp_o.BackgroundColor = Color.White;
                    cp_x.BackgroundColor = Color.Green;
                    plane[c, r] = 2;
                }
                player = !player;
                lines = new int[4];
                Recursion(r, c, -1, -1);
                Recursion(r, c, -1, 0);
                Recursion(r, c, -1, 1);
                Recursion(r, c, 0, -1);
                Recursion(r, c, 0, 1);
                Recursion(r, c, 1, -1);
                Recursion(r, c, 1, 0);
                Recursion(r, c, 1, 1);
                bool action = true;
                if (lines[0] == reachCount || lines[1] == reachCount || lines[2] == reachCount || lines[3] == reachCount)
                {
                    if (plane[c, r] == 1)
                    {
                        action = await DisplayAlert("Dobrá práce", "Vyhrál křížek", "Nová Hra", "Zpět do menu");
                    }
                    else if (plane[c, r] == 2)
                    {
                        action = await DisplayAlert("Dobrá práce", "Vyhrálo kolečko", "Nová Hra", "Zpět do menu");
                    }
                    if (action)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
                else{
                    for (int i = 0; i < plane.GetLength(0); i++)
                    {
                        for (int l = 0; l < plane.GetLength(1); l++)
                        {
                            if (plane[i,l] == 0)
                            {
                                action = false;
                            }
                        }
                    }
                    if (action)
                    {
                        action = await DisplayAlert("Dobrá práce", "Remíza", "Nová Hra", "Zpět do menu");
                        if (action)
                        {
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            await Navigation.PopToRootAsync();
                        }
                    }
                    
                }
            }
        }
        int[] lines = new int[4];
        void Recursion(int x, int y, int kx, int ky)
        {
            int curX = x + kx;
            int curY = y + ky;
            if (curX >= 0 && curX < plane.GetLength(1) && curY >= 0 && curY < plane.GetLength(0))
            {
                if (plane[curY,curX] == plane[y, x])
                {
                    if (ky == 0)
                    {
                        lines[0]++;
                    }
                    else if (kx == ky)
                    {
                        lines[1]++;
                    }
                    else if (kx == 0)
                    {
                        lines[2]++;

                    }
                    else if (kx == ky * -1)
                    {
                        lines[3]++;
                    }
                    Recursion(curX, curY, kx, ky);
                }               
            }
        }
    }
}