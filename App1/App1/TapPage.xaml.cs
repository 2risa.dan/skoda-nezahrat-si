﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class TapPage : ContentPage
    {
        public TapPage()
        {
            InitializeComponent();
        }

        private async void Hra_Pressed(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new skodatap());
        }
    }
}
