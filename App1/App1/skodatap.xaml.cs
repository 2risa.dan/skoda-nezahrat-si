﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class skodatap : ContentPage
    {
        public skodatap()
        {
            InitializeComponent();
            hra();
        }

        Random generator = new Random();
        ImageSource[] auta2 = new ImageSource[4];
        ImageSource[] auta3 = new ImageSource[4];
        int a = 0;
        int b = 0;
        int c = 0;
        int f = 0;
        int g = 0;
        int aktlevel = 1;
        Image[,] pole = new Image[4, 6];
        string[] auta = { "skoda1", "skoda2", "skoda3", "skoda4", "vision1", "vision2", "vision3", "vision4" };

        async void tapped(object sender, EventArgs e)
        {
            Image obraz = sender as Image;
            if (obraz == pole[a, b])
            {
                if (aktlevel == 15)
                {
                    GC.Collect();
                    bool action = true;
                    action = await DisplayAlert("Dobrá práce", "Level: " + aktlevel + "/15", "Nová Hra", "Zpět do menu");
                    if (action)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
                else
                {
                    aktlevel++;
                }
            }
            else
            {
                aktlevel--;
                if (aktlevel == 5 || aktlevel ==10)
                {
                    layout.Children.Remove(maingrid);
                    maingrid = new Grid();
                    maingrid.VerticalOptions = LayoutOptions.CenterAndExpand;
                    layout.Children.Add(maingrid);
                }
                if (aktlevel == 0)
                {
                    GC.Collect();
                    bool action = true;
                    action = await DisplayAlert("Dobrá práce", "Prohrál jsi", "Nová Hra", "Zpět do menu");
                    if (action)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
            }
            for (int i = 0; i < f; i++)
            {
                for (int l = 0; l < g; l++)
                {
                    pole[i, l].Source = null;
                }
            }
            hra();
        }
        void hra()
        {
            level.Text = "Level: " + aktlevel;
            if (aktlevel <= 5)
            {
                f = 2;
                g = 2;
                for (int i = 0; i < 4; i++)
                {
                    auta2[i] = ImageSource.FromFile(auta[i]);
                }
                for (int i = 0; i < 4; i++)
                {
                    auta3[i] = ImageSource.FromFile(auta[i + 4]);
                }
                for (int i = 0; i < f; i++)
                {
                    for (int l = 0; l < g; l++)
                    {
                        pole[i, l] = new Image();
                        pole[i, l].Margin = 10;////
                        pole[i, l].Source = auta2[generator.Next(0, auta2.Length)];
                        maingrid.Children.Add(pole[i, l], i, l);

                        TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
                        tapGestureRecognizer.Tapped += tapped;
                        pole[i, l].GestureRecognizers.Add(tapGestureRecognizer);
                    }
                }

                a = generator.Next(0, 2);
                b = generator.Next(0, 2);
                c = generator.Next(0, 4);
                pole[a, b].Source = auta3[c];
            }
            else if (aktlevel <=10 && aktlevel >5)
            {
                f = 3;
                g = 4;
                for (int i = 0; i < 4; i++)
                {
                    auta2[i] = ImageSource.FromFile(auta[i]);
                }
                for (int i = 0; i < 4; i++)
                {
                    auta3[i] = ImageSource.FromFile(auta[i + 4]);
                }
                for (int i = 0; i < f; i++)
                {
                    for (int l = 0; l < g; l++)
                    {
                        pole[i, l] = new Image();
                        pole[i, l].Margin = 10;////
                        pole[i, l].Source = auta2[generator.Next(0, auta2.Length)];
                        maingrid.Children.Add(pole[i, l], i, l);

                        TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
                        tapGestureRecognizer.Tapped += tapped;
                        pole[i, l].GestureRecognizers.Add(tapGestureRecognizer);
                    }
                }

                a = generator.Next(0, 3);
                b = generator.Next(0, 4);
                c = generator.Next(0, 4);
                pole[a, b].Source = auta3[c];
            }
            else
            {
                f = 4;
                g = 6;
                for (int i = 0; i < 4; i++)
                {
                    auta2[i] = ImageSource.FromFile(auta[i]);
                }
                for (int i = 0; i < 4; i++)
                {
                    auta3[i] = ImageSource.FromFile(auta[i + 4]);
                }
                for (int i = 0; i < f; i++)
                {
                    for (int l = 0; l < g; l++)
                    {
                        pole[i, l] = new Image();
                        pole[i, l].Margin = 5;////
                        pole[i, l].Source = auta2[generator.Next(0, auta2.Length)];
                        maingrid.Children.Add(pole[i, l], i, l);

                        TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
                        tapGestureRecognizer.Tapped += tapped;
                        pole[i, l].GestureRecognizers.Add(tapGestureRecognizer);
                    }
                }

                a = generator.Next(0, 4);
                b = generator.Next(0, 6);
                c = generator.Next(0, 4);
                pole[a, b].Source = auta3[c];
            }
        }
    }
}