﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Simon : ContentPage
    {
        public Simon()
        {
            InitializeComponent();
        }
        private async void Btn_enter_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new hra());
        }
    }
}