﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class hra : ContentPage
    {
        public hra()
        {
            InitializeComponent();
            for (int i = 0; i < moznosti.Length; i++)
            {
                moznosti[i] = new Button();
                Grid.SetRow(moznosti[i], 4);
                Grid.SetColumn(moznosti[i], i);
                moznosti[i].BackgroundColor = barvy[i];
                moznosti[i].Clicked += vyber_click;
                moznosti[i].HeightRequest = 500;
                grid_main.Children.Add(moznosti[i]);

            }
            HideMoznosti();
            Device.StartTimer(TimeSpan.FromMilliseconds(1500), timer);
            game();
        }

        private void HideMoznosti()
        {
            for (int i = 0; i < moznosti.Length; i++)
            {
                if (moznosti[i].IsVisible == true)
                    moznosti[i].IsVisible = false;
            }
        }

        private void ShowMoznosti()
        {
            for (int i = 0; i < moznosti.Length; i++)
            {
                if (moznosti[i].IsVisible == false)
                    moznosti[i].IsVisible = true;
            }
        }

        Color[] barvy =
        {
            Color.FromHex("#FFFF6e00"),
            Color.FromHex("#FFFFFF00"),
            Color.FromHex("#FF0000FF"),
            Color.FromHex("#FF990000"),
            Color.FromHex("#FF00FF00"),
            Color.FromHex("#FFFF00FF"),
            Color.FromHex("#FF000000")
        };
        Button[] moznosti = new Button[7];
        Random generator = new Random();
        int maxsteps = 2;
        int step = 0;
        int level = 1;
        int[] colorsindex = new int[10];
        bool play = false;

        private bool timer()
        {
            return game();
        }

        private bool game()
        {
            if (step >= maxsteps)
            {
                step = 0;
                btn_main.BackgroundColor = Color.Transparent;
                play = true;
                label_info.Text = " ";
                ShowMoznosti();
                return false;
            }

            colorsindex[step] = generator.Next(barvy.Length);
            if (step >= 1)
                while (colorsindex[step - 1] == colorsindex[step])
                    colorsindex[step] = generator.Next(barvy.Length);

            btn_main.BackgroundColor = barvy[colorsindex[step]];
            step++;
            label_info.Text = step + "/" + maxsteps;
            return true;
        }
        private async void vyber_click(object sender, EventArgs e)
        {
            if (play == true)
            {
                btn_main.BackgroundColor = (sender as Button).BackgroundColor;
                if ((sender as Button).BackgroundColor == barvy[colorsindex[step]])
                {
                    step++;
                    if (step >= maxsteps)
                    {
                        label_info.Text = " ";

                        btn_main.BackgroundColor = Color.Transparent;
                        level++;
                        label_level.Text = "Level: " + level;
                        step = 0;
                        maxsteps++;

                        if (maxsteps > colorsindex.Length)
                        {
                            await DisplayAlert("Konec hry", "Vyhrál jsi", "Menu");
                            await Navigation.PopToRootAsync();
                        }
                        else
                        {
                            await DisplayAlert("DOBŘE", "Postupuješ do další úrovně", "Ok");
                            HideMoznosti();
                            Device.StartTimer(TimeSpan.FromMilliseconds(1750), timer);
                        }
                        play = false;
                    }
                    else
                        label_info.Text = "Správně " + step + "/" + maxsteps;
                }
                else
                {
                    HideMoznosti();
                    step = 0;
                    play = false;
                    btn_main.BackgroundColor = Color.Transparent;
                    label_info.Text = "";
                    await DisplayAlert("ŠPATNĚ", "Zkus to znovu", "Ok");
                    Device.StartTimer(TimeSpan.FromMilliseconds(1750), timer);
                }
            }
        }
    }
}