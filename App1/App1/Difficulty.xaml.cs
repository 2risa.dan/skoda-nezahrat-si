﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Difficulty : ContentPage
    {
        public Difficulty()
        {
            InitializeComponent();
            
        }

        string mode = Quiz.mode;

        private async void Btntezka_Pressed(object sender, EventArgs e)
        {
            switch (mode)
            {
                case "znacky": { await Navigation.PushAsync(new znackytezky()); break; }
                case "historie": { await Navigation.PushAsync(new historietezky()); break; }
                default: { break; }
            }
        }

        private async void Btnstredni_Pressed(object sender, EventArgs e)
        {
            switch (mode)
            {
                case "znacky": { await Navigation.PushAsync(new znackystredni()); break; }
                case "historie": { await Navigation.PushAsync(new historiestredni()); break; }
                default: { break; }
            }
        }

        private async void Btnlehka_Pressed(object sender, EventArgs e)
        {
            switch (mode)
            {
                case "znacky": { await Navigation.PushAsync(new znackylehky()); break; }
                case "historie": { await Navigation.PushAsync(new historielehky()); break; }
                default: { break; }
            }
        }
    }
}