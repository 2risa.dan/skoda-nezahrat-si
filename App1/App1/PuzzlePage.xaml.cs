﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class PuzzlePage : ContentPage
    {
        public PuzzlePage()
        {

            int rows = 3;
            int columns = 3;
            InitializeComponent();
            grid_main.HeightRequest = DeviceDisplay.MainDisplayInfo.Width / 2.5;
            grid_main.WidthRequest = DeviceDisplay.MainDisplayInfo.Width / 2.5;

            string[] imagesrcs = { "fabiao", "superbo", "visionxo", "citigoo", "scalao", "fabiarso", "kodiaqrso", "feliciao", "kodiaqrs2o" };
            Image[,] obrazky = new Image[rows, columns];
            string[,] nazvy = new string[rows, columns];
            int id = 0;
            for (int i = 0; i < rows; i++)
                grid_main.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < columns; i++)
                grid_main.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });


            for (int j = 0; j < rows; j++)
            {
                for (int k = 0; k < columns; k++)
                {
                    if (id < imagesrcs.Length)
                    {
                        obrazky[j, k] = new Image();
                        obrazky[j, k].Source = imagesrcs[id];
                        obrazky[j, k].Aspect = Aspect.Fill;
                        nazvy[j, k] = imagesrcs[id].ToString();
                        grid_main.Children.Add(obrazky[j, k], k, j);
                        var tapGestureRecognizer = new TapGestureRecognizer();
                        tapGestureRecognizer.Tapped += async (s, e) =>
                        {
                            await Navigation.PushAsync(new PexesoGame(nazvy[Grid.GetRow(s as Image), Grid.GetColumn(s as Image)]));
                        };
                        id++;
                        obrazky[j, k].GestureRecognizers.Add(tapGestureRecognizer);
                    }
                    else break;
                }
            }
        }
    }
}
