﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class historietezky : ContentPage
    {
        public historietezky()
        {
            InitializeComponent();
            hra();
        }


        Random generator = new Random();
        bool zadano = false;
        string spravna = "X";
        public static bool konechry = false;
        int skore = 0;
        int pokrok = 0;
        int poradi = 0;
        int poradi2 = 0;
        string skoretext = "X";
        int[] used = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        string[] otazky = {
            "Angažuje se ŠKODA AUTO v motoristickém sportu?",
            "Vyrábí ŠKODA AUTO tramvaje?",
            "Kdy se přestala vyrábět Škoda Felicia?",
            "Kolik má ŠKODA AUTO modelů v nabídce? (ke dni 20.5.2019)",
            "Kdo založil firmu ŠKODA AUTO?",
            "Kde je hlavní sídlo ŠKODA AUTO?",
            "Kde jsou závody ŠKODA AUTO?",
            "Kdy byla založena ŠKODA AUTO?",
            "Kolik zaměstnanců pracuje v ŠKODA AUTO?",
            "Jakého koncernu je součástí ŠKODA AUTO?",
            "Jaký je slogan ŠKODA AUTO?",
            "Co bylo prvním výrobkem ŠKODY AUTO?",
            "Jaké mistrovství ŠKODA AUTO sponzoruje?",
            "Jaká barva symbolizuje ŠKODU AUTO?",
            "Je součástí ŠKODA AUTO vzdělávací centrum?",
            "Vyrábí ŠKODA AUTO letadla?",
            "První mírový automobil byl vyroben v roce:",
            "Kdo byli Václav Klement a Václav Laurin?",
            "Jak se jmenoval první model ŠKODY AUTO?",
            "ŠKODU AUTO postihla velká hospodářská krize:",
            "První automobil představila ŠKODA AUTO v roce:",
            "Jak se jmenovala společnost před současným názvem?",
            "Jakou plochu zabírá závod ŠKODA AUTO v Mladé Boleslavi?",
            "Kdo je ředitelem ŠKODA AUTO?",
            "Jak byly modely vyrobeny chronologicky za sebou?",
            "Jaký model je přímým nástupcem ŠKODA RAPID:",
            "Jaký model nepatří mezi modely vyrobené v letech 2008-2015?",
            "Kterého modelu se prodalo nejvíce kusů? (za celou dobu)",
            "Který z uvedených modelů je pohaněn elektřinou?",
            "Jak se nazýval první čtyřkolový automobil?",
            "Co byl originální model Slavia?",
            "Ve kterém roce se firma ŠKODA AUTO spojila s německým koncernem Volkswagen?",
            "S jakým městem se Mladá Boleslav spojila, aby vytvořila firmu ŠKODA AUTO?",
            "V jakém roce ŠKODA AUTO představila model Vision X v Ženevě?",
            "Který z uvedených modelů byl roku 2009 označen jako luxusní auto roku?",
            "Ve které zemi se produkty ŠKODA AUTO nevyrábí?",
            "Jakou zkratku nesl závod firmy ŠKODA AUTO?",
            "Od kterého roku se vyrábí ŠKODA FABIA?",
            "Od kterého roku se vyrábí ŠKODA OCTAVIA?",
            "Od kterého roku se vyrábí ŠKODA SUPERB?",
            "Který model reprezentuje ŠKODU AUTO v Rallye?",
            "Model VISION E má předpokládaný dojezd okolo:",
            "Elektrická verze ŠKODY CITIGO bude v prodeji od roku 2019, jaký má dojezd?",
            "Kolikátá verze ŠKODY OCTAVIA se nyní prodává?",
            "Jakého typu je karosérie modelu KAROQ a KODIAQ?",
            "Kde sídlí ŠKODA AUTO?",
            "Co za typ automobilu byla ŠKODA TUDOR",
            "Jak se přezdívá znaku ŠKODY AUTO?",
            "ŠKODA Muzeum každoročně pořádá akci s názvem?",
            "Při jaké události se vozy ŠKODA vydali až do daleké Indie?",
            "Kam se vydali zástupci ŠKODA AUTO při svém 85. výročí?"
        };

        string[] spravneodpovedi = {
            "ano",
            "ano",
            "2001",
            "8",
            "Václav Laurin",
            "Mladá Boleslav",
            "Mladá Boleslav",
            "1895",
            "33 000 - 36 000",
            "VW",
            "Simply Clever",
            "Kolo",
            "V hokeji",
            "zelená",
            "ano",
            "ne",
            "1945",
            "mechanik a knihkupec",
            "Slavia",
            "za 2. světové války",
            "1905",
            "Laurin a Klement",
            "2,5 km",
            "Bernhard Maier",
            "FABIA, OCTAVIA, SUPERB",
            "SCALA",
            "KODIAQ",
            "OCTAVIA",
            "VISION E",
            "VOLTURETTE",
            "Kolo",
            "1991",
            "Plzeň",
            "2018",
            "SUPERB",
            "Polsko",
            "ASAP",
            "1999",
            "1996",
            "2001",
            "FABIA",
            "550 km",
            "300",
            "3",
            "SUV",
            "Mladá Boleslav",
            "limuzína",
            "okřídlené šípy",
            "Muzejní noc",
            "85. výročí",
            "Kalkata"
        };

        string[] spatneodpovedi = {
            "ne",
            "ne",
            "2018",
            "20",
            "Václav Klement",
            "Praha",
            "Vrchlabí",
            "1990",
            "10 000 - 15 000",
            "BMW",
            "das Auto",
            "automobil",
            "ve fotbale",
            "modrá",
            "ne",
            "ano",
            "1899",
            "závodník a prodavač",
            "OCTAVIA",
            "v roce 2015",
            "1895",
            "Auto Škoda",
            "1,3 km",
            "Tomáš Velek",
            "SUPERB, OCTAVIA, FABIA",
            "YETI",
            "RAPID",
            "FABIA",
            "JOYSTER",
            "420",
            "Čtyřkolový vůz",
            "2004",
            "Pardubice",
            "2019",
            "OCTAVIA",
            "Čína",
            "NSDP",
            "2009",
            "1991",
            "2007",
            "OCTAVIA",
            "400 km",
            "500 km",
            "2",
            "Sportovní automobil",
            "Kvasiny",
            "Nákladní auto",
            "stříbrné šípy",
            "Noc v muzeu",
            "jako dárek za poskytnutí válečné podpory",
            "Bombaj"
        };

        string[] spatneodpovedi2 = {
            "X",
            "X",
            "1990",
            "4",
            "Steve Jobs",
            "Berlín",
            "Kvasiny",
            "1287",
            "100 000 - 110 000",
            "Ford",
            "Passion for Life",
            "letadlo",
            "v tenise",
            "žlutá",
            "X",
            "X",
            "2015",
            "oba byli hudebníci",
            "Monte Carlo",
            "za 1.sv války",
            "2000",
            "Klement a Laurin",
            "5,5 km",
            "Václav Laurin",
            "OCTAVIA, SUPERB, FABIA",
            "KODIAQ",
            "YETI",
            "FAVORIT",
            "RAPID",
            "100",
            "Tříkolový vozidlo",
            "1997",
            "Zlín",
            "2015",
            "YETI",
            "Indie",
            "EŠZP",
            "2002",
            "2005",
            "1995",
            "Felicia",
            "300 km",
            "100 km",
            "4",
            "Kabriolet",
            "Plzeň",
            "Sportovní auto",
            "orlová zelená míle",
            "Zázraky veteránů",
            "jako projev úcty indickému prezidentovi",
            "Nové Dillí"
        };
        string[] spatneodpovedi3 = {
            "X",
            "X",
            "1732",
            "12",
            "Augustus Horch",
            "Kvasiny",
            "Plzeň",
            "1925",
            "5 000 - 7 000",
            "Volvo",
            "Today, tommorow",
            "motocykl",
            "v házený",
            "červená",
            "X",
            "X",
            "2000",
            "Oba byli truhláři",
            "ŠKODA 1203",
            "v roce 1900",
            "1930",
            "Auto Motors",
            "3,25 km",
            "Martin Lundstadt",
            "FABIA, SUPERB, OCTAVIA",
            "KAROQ",
            "CITIGO",
            "105, 120, 130 (dohromady)",
            "YETI",
            "720",
            "Motocykl",
            "1995",
            "Praha",
            "2016",
            "RAPID",
            "Rusko",
            "DDPP",
            "1991",
            "2000",
            "2004",
            "RAPID",
            "800 km",
            "150 km",
            "1",
            "Limuzína",
            "Vrchlabí",
            "Neexistoval",
            "Orlí křídlo",
            "Veletrh ŠKODA automobilů",
            "75. výročí",
            "Hanoj"
        };
        void visible()
        {
            odpoveda.IsVisible = true;
            odpovedb.IsVisible = true;
            odpovedc.IsVisible = true;
            odpovedd.IsVisible = true;
        }

        private void Odpovedb_Pressed(object sender, EventArgs e)
        {
            if (spravna == "b")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        void Odpoveda_Pressed(object sender, EventArgs e)
        {
            if (spravna == "a")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        private void Odpovedc_Pressed(object sender, EventArgs e)
        {
            if (spravna == "c")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        public async void hra()
        {
            if (!zadano == true)
            {
                visible();
                body.Text = "body: " + skore;
                if (pokrok == 5)
                {
                    GC.Collect();
                    bool action = true;
                    action = await DisplayAlert("Dobrá práce", "body: " + skore + "/5", "Nová Hra", "Zpět do menu");
                    if (action)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
                else
                {
                    int id = generator.Next(0, used.Length);
                    while (used[id] == 1)
                    {
                        id = generator.Next(0, used.Length);
                    }
                    Otazka.Text = otazky[id];
                    poradi = generator.Next(1, 5);
                    poradi2 = generator.Next(1, 4);
                    switch (poradi)
                    {
                        case 1:
                            {
                                odpoveda.Text = spravneodpovedi[id];
                                switch (poradi2)
                                {
                                    case 1: odpovedb.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedc.Text = spatneodpovedi2[id]; odpovedd.Text = spatneodpovedi3[id]; } else { odpovedc.IsVisible = false; odpovedd.IsVisible = false; } break;
                                    case 2: odpovedc.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedb.Text = spatneodpovedi2[id]; odpovedd.Text = spatneodpovedi3[id]; } else { odpovedb.IsVisible = false; odpovedd.IsVisible = false; } break;
                                    case 3: odpovedd.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedb.Text = spatneodpovedi2[id]; odpovedc.Text = spatneodpovedi3[id]; } else { odpovedb.IsVisible = false; odpovedc.IsVisible = false; } break;
                                }
                                spravna = "a";
                                break;
                            }
                        case 2:
                            {
                                odpovedb.Text = spravneodpovedi[id];
                                switch (poradi2)
                                {
                                    case 1: odpoveda.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedc.Text = spatneodpovedi2[id]; odpovedd.Text = spatneodpovedi3[id]; } else { odpovedc.IsVisible = false; odpovedd.IsVisible = false; } break;
                                    case 2: odpovedc.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedd.Text = spatneodpovedi2[id]; odpoveda.Text = spatneodpovedi3[id]; } else { odpovedd.IsVisible = false; odpoveda.IsVisible = false; } break;
                                    case 3: odpovedd.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpoveda.Text = spatneodpovedi2[id]; odpovedc.Text = spatneodpovedi3[id]; } else { odpoveda.IsVisible = false; odpovedc.IsVisible = false; } break;
                                }
                                spravna = "b";
                                break;
                            }
                        case 3:
                            {
                                odpovedc.Text = spravneodpovedi[id];
                                switch (poradi2)
                                {
                                    case 1: odpoveda.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedb.Text = spatneodpovedi2[id]; odpovedd.Text = spatneodpovedi3[id]; } else { odpovedb.IsVisible = false; odpovedd.IsVisible = false; } break;
                                    case 2: odpovedb.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpoveda.Text = spatneodpovedi2[id]; odpovedd.Text = spatneodpovedi3[id]; } else { odpoveda.IsVisible = false; odpovedd.IsVisible = false; } break;
                                    case 3: odpovedd.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedb.Text = spatneodpovedi2[id]; odpoveda.Text = spatneodpovedi3[id]; } else { odpovedb.IsVisible = false; odpoveda.IsVisible = false; } break;
                                }
                                spravna = "c";
                                break;
                            }
                        case 4:
                            {
                                odpovedd.Text = spravneodpovedi[id];
                                switch (poradi2)
                                {
                                    case 1: odpoveda.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedb.Text = spatneodpovedi2[id]; odpovedc.Text = spatneodpovedi3[id]; } else { odpovedb.IsVisible = false; odpovedc.IsVisible = false; } break;
                                    case 2: odpovedb.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpoveda.Text = spatneodpovedi2[id]; odpovedc.Text = spatneodpovedi3[id]; } else { odpoveda.IsVisible = false; odpovedc.IsVisible = false; } break;
                                    case 3: odpovedc.Text = spatneodpovedi[id]; if (spatneodpovedi2[id] != "X") { odpovedb.Text = spatneodpovedi2[id]; odpoveda.Text = spatneodpovedi3[id]; } else { odpovedb.IsVisible = false; odpoveda.IsVisible = false; } break;
                                }
                                spravna = "d";
                                break;
                            }
                    }
                    used[id] = 1;
                    pokrok++;
                    skoretext = pokrok + "/5";
                    progress.Text = skoretext;
                    zadano = true;
                }
            }
        }
    }
}
