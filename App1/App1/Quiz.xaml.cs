﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Quiz : ContentPage
    {
        public Quiz()
        {
            InitializeComponent();
        }
        public static string mode = null;
        private async void Btnznacky_Pressed(object sender, EventArgs e)
        {
            mode = "znacky";
            await Navigation.PushAsync(new Difficulty());
        }

        private async void Btnhistorie_Pressed(object sender, EventArgs e)
        {
            mode = "historie";
            await Navigation.PushAsync(new Difficulty());
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }
    }
}