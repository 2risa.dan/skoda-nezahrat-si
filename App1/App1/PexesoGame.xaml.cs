﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PexesoGame : ContentPage
    {
        int id = 0;

        int[] randomindx = new int[16];
        int[] imgs = new int[16];
        Random generator = new Random();
        int clickedrow = 0;
        int clickedcolumn = 0;
        bool clicked = false;
        Image[,] obrazky = new Image[4, 4];
        Image nahled = new Image();
        Frame ramecek = new Frame();
        public PexesoGame(string folder)
        {
            InitializeComponent();
            grid_main.HeightRequest = DeviceDisplay.MainDisplayInfo.Width / 2.5;
            grid_main.WidthRequest = DeviceDisplay.MainDisplayInfo.Width / 2.5;

            nahled = new Image();
            nahled.Source = folder;
            ramecek.HeightRequest = DeviceDisplay.MainDisplayInfo.Width / 8;
            ramecek.WidthRequest = DeviceDisplay.MainDisplayInfo.Width / 8;
            ramecek.Padding = 4;
            ramecek.Margin = 20;
            ramecek.CornerRadius = 5;
            ramecek.HorizontalOptions = LayoutOptions.CenterAndExpand;
            ramecek.BackgroundColor = Color.Black;
            ramecek.BorderColor = Color.Black;
            ramecek.Content = nahled;
            layout_main.Children.Add(ramecek);


            GenerateNewLayout();
            RenderImages(folder);

        }

        private void RenderImages(string folder)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    obrazky[i, j] = new Image();
                    obrazky[i, j].Source = folder + "brazek" + imgs[id] + ".png";
                    grid_main.Children.Add(obrazky[i, j]);
                    obrazky[i, j].Aspect = Aspect.Fill;
                    Grid.SetColumn(obrazky[i, j], i);
                    Grid.SetRow(obrazky[i, j], j);
                    var tapGestureRecognizer = new TapGestureRecognizer();
                    tapGestureRecognizer.Tapped += async (s, e) =>
                    {
                        if (clicked == false)
                        {
                            clickedcolumn = Grid.GetColumn(s as Image);
                            clickedrow = Grid.GetRow(s as Image);
                            (s as Image).Opacity = 0.8;
                            clicked = true;
                        }
                        else if (clicked == true)
                        {
                            int row = Grid.GetRow(s as Image);
                            int column = Grid.GetColumn(s as Image);
                            Grid.SetRow(obrazky[clickedcolumn, clickedrow], Grid.GetRow(s as Image));
                            Grid.SetColumn(obrazky[clickedcolumn, clickedrow], Grid.GetColumn(s as Image));
                            obrazky[column, row] = obrazky[clickedcolumn, clickedrow];
                            obrazky[column, row].Opacity = 1;
                            obrazky[clickedcolumn, clickedrow] = s as Image;
                            Grid.SetRow(s as Image, clickedrow);
                            Grid.SetColumn(s as Image, clickedcolumn);

                            clicked = false;
                        }
                        await CheckForWinAsync(folder);
                    };
                    obrazky[i, j].GestureRecognizers.Add(tapGestureRecognizer);
                    id++;
                }
            }
        }
        private void GenerateNewLayout()
        {
            for (int i = 0; i < randomindx.Length; i++)
            {
                int c = generator.Next(1, randomindx.Length + 1);
                while (randomindx.Contains(c))
                {
                    c = generator.Next(1, randomindx.Length + 1);
                }
                imgs[i] = c;
                randomindx[i] = c;
            }
        }
        private async Task CheckForWinAsync(string folder)
        {
            bool x = true;
            int cisla = 0;
            for (int o = 0; o < 4; o++)
            {
                for (int l = 0; l < 4; l++)
                {
                    cisla++;
                    if (obrazky[l, o].Source.ToString() != "File: " + folder + "brazek" + cisla + ".png")
                    {
                        x = false;
                        break;
                    }
                }
            }
            if (x)
            {
                await DisplayAlert("Konec hry", "Obrázek složen", "Zpět do menu");
                await Navigation.PopToRootAsync();
            }
        }
    }
}