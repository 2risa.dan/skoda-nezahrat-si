﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async private void Kviz_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Quiz());
        }
        async private void Pexeso_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PexesoPage());
        }
        async private void Gomoku_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GomokuPage());
        }
        async private void Simon_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Simon());
        }
        async private void Credits_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Credits());
        }
        async private void Puzzle_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PuzzlePage());
        }
        async private void Skodatap_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TapPage());
        }
    }
}
