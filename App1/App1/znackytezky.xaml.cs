﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class znackytezky : ContentPage
    {
        public znackytezky()
        {
            InitializeComponent();
            znacky2 = new ImageSource[znacky.Length];
            for (int i = 0; i < znacky.Length; i++)
            {
                znacky2[i] = ImageSource.FromFile(znacky[i]);

            }
            hra();
        }

        Random generator = new Random();
        ImageSource[] znacky2;
        bool zadano = false;
        string spravna = "X";
        int skore = 0;
        int id = 0;
        int pokrok = 0;
        int poradi = 0;
        int poradi2 = 0;
        int jinaznacka = 0;
        int jinaznacka2 = 0;
        int jinaznacka3 = 0;
        string skoretext = "X";
        int[] used = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
        string[] otazky = {
        "Vyber značku, která znázorňuje maximální povolenou rychlost:",
        "Která značka zakazuje vjezd osobním vozidlům?",
        "Vyber značku která označuje parkoviště s parkovacím automatem:",
        "Která značka znázorňuje autobusovou zastávku?",
        "Vyber značku, která označuje dálnici:",
        "Vyber značku, která označuje hlavní silnici:",
        "Která značka zakazuje vstup chodcům?",
        "Která značka zakazuje vjezd všech vozidel?",
        "Která značka upozorňuje na možný výskyt zeminy nebo kamení na pozemní komunikaci?",
        "Vyber značku, která zakazuje vjezd cyklistům:",
        "Vyber značku, která upozorňuje na křižovatku s vedlejší pozemní komunikací:",
        "Která značka upozorňuje na kruhový objezd?",
        "Která značka zakazuje odbočení vlevo?",
        "Vyber značku, která znázorňuje parkoviště:",
        "Vyber značku, která upozorňuje na práci na silnici:",
        "Která značka zakazuje předjíždět?",
        "Vyber značku, která přikazuje dát přednost v jízdě:",
        "Která značka upozorňuje na přechod?",
        "Vyber značků, která upozorňuje na železniční přejezd bez závor:",
        "Která značka označuje benzínovou pumpu?",
        "Která značka označuje silnici pro motorová vozidla?",
        "Vyber značku označující slepou ulici:",
        "Která značka upozorňuje nebezpečí smyku?",
        "Která značka přikazuje zastavit vozidlo a dát přednost v jízdě?",
        "Vyber značku, která zakazuje vjezd všem vozidlům v obou směrech:"
        };
        string[] znacky = { "rychlost", "auto", "automat", "bus", "dalnice", "hlavni", "chodec", "jednosmerka", "kameni", "kolo", "krizovatka", "kruhac", "odboceni", "parkoviste", "prace", "predjizdeni", "prednost", "prechod", "prejezd", "pumpa", "silnice", "slepa", "smyk", "stop", "zakaz" };

        private void Znacka1_Clicked(object sender, EventArgs e)
        {
            if (spravna == "a")
            {
                skore++;                
            }
            zadano = false;
            hra();
        }

        private void Znacka2_Clicked(object sender, EventArgs e)
        {
            if (spravna == "b")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        private void Znacka3_Clicked(object sender, EventArgs e)
        {
            if (spravna == "c")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        private void Znacka4_Clicked(object sender, EventArgs e)
        {
            if (spravna == "d")
            {
                skore++;
            }
            zadano = false;
            hra();
        }
        public async void hra()
        {
            if (!zadano == true)
            {
                body.Text = "body: " + skore;
                if (pokrok == 5)
                {
                    GC.Collect();
                    bool action = true;
                    action = await DisplayAlert("Dobrá práce", "body: " + skore + "/5", "Nová Hra", "Zpět do menu");
                    if (action)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
                else
                {
                    id = generator.Next(0, 25);
                    while (used[id] == 1)
                    {
                        id = generator.Next(0, 25);
                    }
                    Otazka.Text = otazky[id];
                    poradi = generator.Next(1, 5);
                    poradi2 = generator.Next(1, 4);
                    while (jinaznacka == id)
                    {
                        jinaznacka = generator.Next(0, 25);
                    }
                    while (jinaznacka2 == id || jinaznacka2 == jinaznacka)
                    {
                        jinaznacka2 = generator.Next(0, 25);
                    }
                    while (jinaznacka3 == id || jinaznacka3 == jinaznacka2 || jinaznacka3 == jinaznacka)
                    {
                        jinaznacka3 = generator.Next(0, 25);
                    }
                    switch (poradi)
                    {
                        case 1:
                            {
                                znacka1.Source = znacky[id];
                                switch (poradi2)
                                {
                                    case 1: znacka2.Source = znacky[jinaznacka]; znacka3.Source = znacky[jinaznacka2]; znacka4.Source = znacky[jinaznacka3]; break;
                                    case 2: znacka3.Source = znacky[jinaznacka]; znacka2.Source = znacky[jinaznacka2]; znacka4.Source = znacky[jinaznacka3]; break;
                                    case 3: znacka4.Source = znacky[jinaznacka]; znacka2.Source = znacky[jinaznacka2]; znacka3.Source = znacky[jinaznacka3]; break;
                                }
                                spravna = "a";
                                break;
                            }
                        case 2:
                            {
                                znacka2.Source = znacky[id];
                                switch (poradi2)
                                {
                                    case 1: znacka1.Source = znacky[jinaznacka]; znacka3.Source = znacky[jinaznacka2]; znacka4.Source = znacky[jinaznacka3]; break;
                                    case 2: znacka3.Source = znacky[jinaznacka]; znacka1.Source = znacky[jinaznacka2]; znacka4.Source = znacky[jinaznacka3]; break;
                                    case 3: znacka3.Source = znacky[jinaznacka]; znacka4.Source = znacky[jinaznacka2]; znacka1.Source = znacky[jinaznacka3]; break;
                                }
                                spravna = "b";
                                break;
                            }
                        case 3:
                            {
                                znacka3.Source = znacky[id];
                                switch (poradi2)
                                {
                                    case 1: znacka2.Source = znacky[jinaznacka]; znacka1.Source = znacky[jinaznacka2]; znacka4.Source = znacky[jinaznacka3]; break;
                                    case 2: znacka1.Source = znacky[jinaznacka]; znacka2.Source = znacky[jinaznacka2]; znacka4.Source = znacky[jinaznacka3]; break;
                                    case 3: znacka4.Source = znacky[jinaznacka]; znacka2.Source = znacky[jinaznacka2]; znacka1.Source = znacky[jinaznacka3]; break;
                                }
                                spravna = "c";
                                break;
                            }
                        case 4:
                            {
                                znacka4.Source = znacky[id];
                                switch (poradi2)
                                {
                                    case 1: znacka2.Source = znacky[jinaznacka]; znacka1.Source = znacky[jinaznacka2]; znacka3.Source = znacky[jinaznacka3]; break;
                                    case 2: znacka1.Source = znacky[jinaznacka]; znacka2.Source = znacky[jinaznacka2]; znacka3.Source = znacky[jinaznacka3]; break;
                                    case 3: znacka3.Source = znacky[jinaznacka]; znacka2.Source = znacky[jinaznacka2]; znacka1.Source = znacky[jinaznacka3]; break;
                                }
                                spravna = "d";
                                break;
                            }

                    }
                    used[id] = 1;
                    pokrok++;
                    skoretext = pokrok + "/5";
                    progress.Text = skoretext;
                    zadano = true;
                }
            }
        }
    }
}