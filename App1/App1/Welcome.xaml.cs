﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Welcome : ContentPage
    {
        public Welcome()
        {
            InitializeComponent();
            Device.StartTimer(TimeSpan.FromSeconds(2), timer);
        }

        bool timer ()
        {
            App.Current.MainPage = new NavigationPage(new MainPage());
            return false;
        }
    }
}