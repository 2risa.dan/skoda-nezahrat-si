﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Game : ContentPage
    {
        Random random = new Random();
        ImageSource logo = ImageSource.FromFile("iicologo");
        List<string> source = new List<string>();
        ImageSource[,] sourceArray;
        ImageButton current = null;
        ImageButton current2 = null;
        bool waitForTap = false;
        int rows = 0;
        int colums = 4;
        int moves = 0;
        public Game(object sender)
        {
            InitializeComponent();
            grid.HeightRequest = DeviceDisplay.MainDisplayInfo.Width;
            source.Add("fabia");
            source.Add("fabia2");
            source.Add("favorit");
            source.Add("felicia");
            source.Add("kamiq");
            source.Add("karoq");
            source.Add("karoq2");
            source.Add("octavia2kombi");
            source.Add("octavia2");
            source.Add("octavia3");
            source.Add("octavia4");
            source.Add("scala");
            source.Add("superb");
            source.Add("superb2");
            source.Add("vision");
            source.Add("vision5");
            source.Add("citigo2");
            source.Add("superbscout");
            source.Add("roomster");
            source.Add("skoda860");
            source.Add("yeti");
            source.Add("rapid");
            source.Add("skoda110");
            source.Add("skoda110R");
            source.Add("voiturette");
            source.Add("tudor");
            source.Add("fabiars5");
            source.Add("superb3");
            source.Add("citigo");
            Image img = sender as Image;
            Debug.WriteLine("___" + img.Source + "___" + img.ClassId);
            switch (img.ClassId)
            {
                case "easy": rows = 4; break;
                case "medium": rows = 6; break;
                case "hard": rows = 8; break;
                default: break;
            }
            List<ImageSource> selected = new List<ImageSource>();
            for (int i = 0; i < rows * colums / 2; i++)
            {
                string str = source[random.Next(0, source.Count())];
                ImageSource s = ImageSource.FromFile(str);
                selected.Add(s);
                selected.Add(s);
                source.Remove(str);
            }
            Debug.WriteLine("___" + selected.Count());
            sourceArray = new ImageSource[colums, rows];

            for (int i = 0; i < colums; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }
            for (int i = 0; i < rows; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            for (int i = 0; i < colums; i++)
            {
                for (int l = 0; l < rows; l++)
                {
                    ImageButton bt = new ImageButton();
                    bt.Padding = 5;
                    bt.Source = logo;
                    bt.Clicked += Clicked;
                    grid.Children.Add(bt, i, l);
                    ImageSource s = selected[random.Next(0, selected.Count())];
                    sourceArray[i, l] = s;
                    selected.Remove(s);
                }
            }
            source.Clear();
            selected.Clear();
            GC.Collect();
        }
        void SetLogo()
        {
            current2.Source = logo;
            current.Source = logo;
            current2 = null;
            current = null;
            waitForTap = false;
        }
        async private void Clicked(object sender, EventArgs e)
        {
            ImageButton bt = sender as ImageButton;
            int c = (int)bt.GetValue(Grid.ColumnProperty);
            int r = (int)bt.GetValue(Grid.RowProperty);
            
            if (waitForTap)
            {
                SetLogo();
                return;
            }
            bt.Source = sourceArray[c, r];            
            if (bt.Source == null)
            {
                return;
            }
            if (current == null)
            {
                current = bt;
            }
            else
            {
                if (bt.Source == current.Source && bt !=current)
                {
                    sourceArray[c, r] = null;
                    sourceArray[(int)current.GetValue(Grid.ColumnProperty), (int)current.GetValue(Grid.RowProperty)] = null;
                    ImageButton tmp = current ;
                    current = null;
                    Device.StartTimer(TimeSpan.FromMilliseconds(1000), () => {
                        bt.BackgroundColor = Color.White;
                        bt.Source = null;
                        tmp.Source = null;
                        tmp.BackgroundColor = Color.White;
                        //current = null;
                        return false;
                    });
                }
                else
                {
                    waitForTap = true;
                    current2 = bt;
                }
                moves++;
            }
            bool check = true;
            for (int i = 0; i < colums; i++)
            {
                for (int l = 0; l < rows; l++)
                {
                    if (sourceArray[i, l] !=null)
                    {
                        check = false;
                    }
                }
            }
            if (check)
            {
                check = await DisplayAlert("Dobrá práce", "Výborně", "Nová Hra", "Zpět do menu");
                if (!check)
                {
                    await Navigation.PopToRootAsync();
                }
                else
                {
                    await Navigation.PopAsync();
                }
            }
        }
    }
}