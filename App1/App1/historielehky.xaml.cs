﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class historielehky : ContentPage
    {
        public historielehky()
        {
            InitializeComponent();
            hra();
        }

        Random generator = new Random();
        bool zadano = false;
        string spravna = "X";
        int skore = 0;
        int pokrok = 0;
        string skoretext = "X";
        int[] used = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        string[] otazky = {
            "Angažuje se ŠKODA AUTO v motoristickém sportu?",
            "Vyrábí ŠKODA AUTO tramvaje?",
            "Kdy se přestala vyrábět Škoda Felicia?",
            "Kolik má ŠKODA AUTO modelů v nabídce? (ke dni 20.5.2019)",
            "Kdo založil firmu ŠKODA AUTO?",
            "Kde je hlavní sídlo ŠKODA AUTO?",
            "Kde jsou závody ŠKODA AUTO?",
            "Kdy byla založena ŠKODA AUTO?",
            "Kolik zaměstnanců pracuje v ŠKODA AUTO?",
            "Jakého koncernu je součástí ŠKODA AUTO?",
            "Jaký je slogan ŠKODA AUTO?",
            "Co bylo prvním výrobkem ŠKODY AUTO?",
            "Jaké mistrovství ŠKODA AUTO sponzoruje?",
            "Jaká barva symbolizuje ŠKODU AUTO?",
            "Je součástí ŠKODA AUTO vzdělávací centrum?",
            "Vyrábí ŠKODA AUTO letadla?",
            "První mírový automobil byl vyroben v roce:",
            "Kdo byli Václav Klement a Václav Laurin?",
            "Jak se jmenoval první model ŠKODY AUTO?",
            "ŠKODU AUTO postihla velká hospodářská krize:",
            "První automobil představila ŠKODA AUTO v roce:",
            "Jak se jmenovala společnost před současným názvem?",
            "Jakou plochu zabírá závod ŠKODA AUTO v Mladé Boleslavi?",
            "Kdo je ředitelem ŠKODA AUTO?",
            "Jak byly modely vyrobeny chronologicky za sebou?",
            "Jaký model je přímým nástupcem ŠKODA RAPID:",
            "Jaký model nepatří mezi modely vyrobené v letech 2008-2015?",
            "Kterého modelu se prodalo nejvíce kusů? (za celou dobu)",
            "Který z uvedených modelů je pohaněn elektřinou?",
            "Jak se nazýval první čtyřkolový automobil?",
            "Co byl originální model Slavia?",
            "Ve kterém roce se firma ŠKODA AUTO spojila s německým koncernem Volkswagen?",
            "S jakým městem se Mladá Boleslav spojila, aby vytvořila firmu ŠKODA AUTO?",
            "V jakém roce ŠKODA AUTO představila model Vision X v Ženevě?",
            "Který z uvedených modelů byl roku 2009 označen jako luxusní auto roku?",
            "Ve které zemi se produkty ŠKODA AUTO nevyrábí?",
            "Jakou zkratku nesl závod firmy ŠKODA AUTO?",
            "Od kterého roku se vyrábí ŠKODA FABIA?",
            "Od kterého roku se vyrábí ŠKODA OCTAVIA?",
            "Od kterého roku se vyrábí ŠKODA SUPERB?",
            "Který model reprezentuje ŠKODU AUTO v Rallye?",
            "Model VISION E má předpokládaný dojezd okolo:",
            "Elektrická verze ŠKODY CITIGO bude v prodeji od roku 2019, jaký má dojezd?",
            "Kolikátá verze ŠKODY OCTAVIA se nyní prodává?",
            "Jakého typu je karosérie modelu KAROQ a KODIAQ?",
            "Kde sídlí ŠKODA AUTO?",
            "Co za typ automobilu byla ŠKODA TUDOR",
            "Jak se přezdívá znaku ŠKODY AUTO?",
            "ŠKODA Muzeum každoročně pořádá akci s názvem?",
            "Při jaké události se vozy ŠKODA vydali až do daleké Indie?",
            "Kam se vydali zástupci ŠKODA AUTO při svém 85. výročí?"
        };

        string[] spravneodpovedi = {
            "ano",
            "ano",
            "2001",
            "8",
            "Václav Laurin",
            "Mladá Boleslav",
            "Mladá Boleslav",
            "1895",
            "33 000 - 36 000",
            "VW",
            "Simply Clever",
            "Kolo",
            "V hokeji",
            "zelená",
            "ano",
            "ne",
            "1945",
            "mechanik a knihkupec",
            "Slavia",
            "za 2. světové války",
            "1905",
            "Laurin a Klement",
            "2,5 km",
            "Bernhard Maier",
            "FABIA, OCTAVIA, SUPERB",
            "SCALA",
            "KODIAQ",
            "OCTAVIA",
            "VISION E",
            "VOLTURETTE",
            "Kolo",
            "1991",
            "Plzeň",
            "2018",
            "SUPERB",
            "Polsko",
            "ASAP",
            "1999",
            "1996",
            "2001",
            "FABIA",
            "550 km",
            "300",
            "3",
            "SUV",
            "Mladá Boleslav",
            "limuzína",
            "okřídlené šípy",
            "Muzejní noc",
            "85. výročí",
            "Kalkata"
        };

        string[] spatneodpovedi = {
            "ne",
            "ne",
            "2018",
            "20",
            "Václav Klement",
            "Praha",
            "Vrchlabí",
            "1990",
            "10 000 - 15 000",
            "BMW",
            "das Auto",
            "automobil",
            "ve fotbale",
            "modrá",
            "ne",
            "ano",
            "1899",
            "závodník a prodavač",
            "OCTAVIA",
            "v roce 2015",
            "1895",
            "Auto Škoda",
            "1,3 km",
            "Tomáš Velek",
            "SUPERB, OCTAVIA, FABIA",
            "YETI",
            "RAPID",
            "FABIA",
            "JOYSTER",
            "420",
            "Čtyřkolový vůz",
            "2004",
            "Pardubice",
            "2019",
            "OCTAVIA",
            "Čína",
            "NSDP",
            "2009",
            "1991",
            "2007",
            "OCTAVIA",
            "400 km",
            "500 km",
            "2",
            "Sportovní automobil",
            "Kvasiny",
            "Nákladní auto",
            "stříbrné šípy",
            "Noc v muzeu",
            "jako dárek za poskytnutí válečné podpory",
            "Bombaj"
        };

        void Odpoveda_Pressed(object sender, EventArgs e)
        {
            if (spravna == "a")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        private void Odpovedb_Pressed(object sender, EventArgs e)
        {
            if (spravna == "b")
            {
                skore++;
            }
            zadano = false;
            hra();
        }

        public async void hra()
        {
            if (!zadano == true)
            {
                if (pokrok == 5)
                {
                    GC.Collect();
                    bool action = true;
                    action = await DisplayAlert("Dobrá práce", "body: " + skore + "/5", "Nová Hra", "Zpět do menu");
                    if (action)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await Navigation.PopToRootAsync();
                    }
                }
                else
                {
                    int id = generator.Next(0, used.Length);
                    while (used[id] == 1)
                    {
                        id = generator.Next(0, used.Length);
                    }
                    Otazka.Text = otazky[id];
                    if (id % 2 == 0)
                    {
                        odpoveda.Text = spravneodpovedi[id];
                        odpovedb.Text = spatneodpovedi[id];
                        spravna = "a";
                    }
                    else
                    {
                        odpovedb.Text = spravneodpovedi[id];
                        odpoveda.Text = spatneodpovedi[id];
                        spravna = "b";
                    }
                    used[id] = 1;
                    pokrok++;
                    skoretext = pokrok + "/5";
                    body.Text = "body: " + skore;
                    progress.Text = skoretext;
                    zadano = true;
                }
            }
        }
    }
}